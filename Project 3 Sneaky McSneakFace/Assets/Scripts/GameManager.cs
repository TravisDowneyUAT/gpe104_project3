﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject playerPrefab; //reference to the player prefab
    public GameObject guardPrefab; //reference to the guard prefab
    public GameObject docPrefab; //reference to the document prefab
    [HideInInspector]
    public GameObject exit; //reference to the exit door game object in the level

    public static GameManager instance; //static reference to the game manager object

    public int playerLives; //int to hold the player's lives
    private int documentsRemaining; //int to hold the amount of documents remaining for the player to collect

    public Text playerLivesText; //text object to display the amount of lives the player has
    public Text documentsRemainingText; //text object to display the amount of documents re remaining

    private void Awake()
    { 
        /*if there is no instance of the game manager object then make this instance the new instance
         and don't destroy this game object on load
         otherwise destroy this game object*/
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        //set the exit reference to the exit object in the scene with the tag of Finish
        exit = GameObject.FindGameObjectWithTag("Finish");
        //spawn in the player prefab at -9 on the x-axis with a rotation of -90 of the z-axis
        Instantiate(playerPrefab, new Vector3(-9, 0, 0), Quaternion.Euler(0, 0, -90));
        //spawn in the guard prefab at 7 on the x-axis and -1 on the y-axis with a standard rotation
        Instantiate(guardPrefab, new Vector3(7, -1, 0), Quaternion.Euler(0, 0, 0));
        //start the game
        GameStart();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void GameStart()
    {
        //fill the health bar of the guard
        guardPrefab.GetComponent<AIPawn>().healthBar.fillAmount = 1f / 1f;

        playerLives = 1; //set the player's lives equal to 1
        documentsRemaining = 5; //set the amount of documents remaining equal to 5

        playerLivesText.text = "Lives: " + playerLives; //update the player's lives based on the playerLives variable
        documentsRemainingText.text = "Documents: " + documentsRemaining; //update the documents remaining based on the documentsRemaining variable

        //set the ExitTrigger component on the exit door to false
        exit.GetComponent<ExitTrigger>().enabled = false;

        //spawn in the documents
        SpawnDocs();
    }

    void SpawnDocs()
    {
        /*Instantiate a document at the following coordinates in the level
         (I spawned the documents on the same positions as the waypoints so it makes it
         tougher for the player to get to each document, change the Vectors accordingly)*/
        Instantiate(docPrefab, new Vector3(7f, 3f, 0f), Quaternion.identity);
        Instantiate(docPrefab, new Vector3(7f, -3f, 0f), Quaternion.identity);
        Instantiate(docPrefab, new Vector3(0f, -0.1f, 0f), Quaternion.identity);
        Instantiate(docPrefab, new Vector3(-5f, 3f, 0f), Quaternion.identity);
        Instantiate(docPrefab, new Vector3(-5f, -2f, 0f), Quaternion.identity);
    }

    public void DecreaseDocs()
    {
        //decrease the amount of documents remaining by 1
        documentsRemaining--;

        //update the UI to display the new amount of documents remaining
        documentsRemainingText.text = "Documents: " + documentsRemaining;

        //if the amount of documents remaining is equal to 0 activate the ExitTrigger component of the exit door
        if(documentsRemaining <= 0)
        {
            exit.GetComponent<ExitTrigger>().enabled = true;
        }
    }

    public void DecreaseLives()
    {
        //decrease the player's lives by 1
        playerLives--;

        //update the UI to display the new amount of lives remaining
        playerLivesText.text = "Lives: " + playerLives;

        //if the player's lives drop below 1 then load the GameOver scene
        if(playerLives < 1)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
