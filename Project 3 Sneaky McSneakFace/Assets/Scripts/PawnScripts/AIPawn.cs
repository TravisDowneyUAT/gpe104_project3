﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AIPawn : Pawn
{
    [HideInInspector]
    public GameObject player; //reference to the player prefab
    

    //Patrol variables
    public Transform[] waypoints; //create a Transform array to hold the positions of the waypoints
    private int randomPoint; //make an int variable to hold a random point from the waypoints array
    private float waitTime; //make a float variable to determine how long the guard will wait at each waypoint
    public float startWaitTime; //make a float variable to pubically influence the wait time of the guard

    //Chase variables
    public float moveSpeed; //float variable that tells the guard how fast to move each frame draw

    // Start is called before the first frame update
    void Start()
    {
        viewDis = 5f; //set the view distance of the pawn to 5
        giveUpChaseDis = 5f; //set the give up chase distance of the pawn to 5
        tf = GetComponent<Transform>(); //set the tf variable to the guard's transform
        player = GameObject.FindGameObjectWithTag("Player"); //set the player variable to get the info of the player prefab
        FindNextWaypoint(); //find the next waypoint for the guard to move to
        waitTime = startWaitTime; //set the wait time equal to whatever we set in the inspector
        isPatrol = true; //have isPatrol start off as true
        isChase = false; //have isChase start off as false
        inSight = false; //have inSight start off as false
        health = 4; //set the guard's health equal to 4
        //get the health bar image and set it to the child of a child object of the guard
        healthBar = GetComponentInChildren<Image>().GetComponentInChildren<Image>();

        //set the fov variable to the FieldOfView component of the instantiated FOV prefab
        fov = Instantiate(pfFOV, null).GetComponent<FieldOfView>();
    }

    private void Update()
    {
       
    }

    public override void FSM()
    {
        /*if the guard is patrolling then run the Patrol method
         else if the player is in sight then run the chase method*/
        if(isPatrol == true)
        {
            Patrol();
        }else if( inSight == true)
        {
            Chase();
        }
    }

    public override void Patrol()
    {
        Debug.Log("The Patrol method is firing");
        //set the guard's position to move towards the chosen random waypoints position and get there based on the move speed variable
        tf.position = Vector2.MoveTowards(tf.position, waypoints[randomPoint].position, moveSpeed * Time.deltaTime);
        //set the guard to look towards the waypoint as he moves to it
        tf.up = waypoints[randomPoint].position - tf.position;
        /*if the distance from the guard to the waypoint is less than 0.2
         and if the wait time is less than or equal to 0 find another waypoint and reset the wait time to starting wait time
         otherwise have the waitime decrease until it hits 0*/
        if(Vector2.Distance(tf.position, waypoints[randomPoint].position) < 0.2f)
        {
            if(waitTime <= 0)
            {
                FindNextWaypoint();
                waitTime = startWaitTime;
            }else
            {
                waitTime -= Time.deltaTime;
            }
        }
        //if the distance between the player and the guard is less than the view distance set inSight equal to true
        if(Vector2.Distance(tf.position,player.transform.position) < viewDis)
        {
            inSight = true;
        }
    }

    public override void FindNextWaypoint()
    {
        Debug.Log("Finding next waypoint");
        //find another waypoint by getting a random point from the waypoints array
        randomPoint = Random.Range(0, waypoints.Length);
    }

    public override void Chase()
    {
        Debug.Log("The Chase state is firing");
        /*if the distance between the player and the guard is greater than the give up distance
         then have the guard move towards the player based on the move speed variable
         otherwise set inSight back to false*/
        if(Vector2.Distance(tf.position,player.transform.position) > giveUpChaseDis)
        {
            tf.position = Vector2.MoveTowards(tf.position, player.transform.position, moveSpeed * Time.deltaTime);
        }
        else
        {
            inSight = false;
        }
    }

    public override void TakeDamage(int damage)
    {
        //take damage by decreasing the health based on the damage given
        health -= damage;
        //fill the health bar based on the amount of health left
        healthBar.fillAmount = health / 4f;

        //if the health is reduced to 0 then destroy the game object
        if(health <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
