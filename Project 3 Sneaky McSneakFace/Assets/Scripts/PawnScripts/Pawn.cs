﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Pawn : MonoBehaviour
{
    public Transform tf; //reference to the individual pawn transform
    public float hearingScale; //float variable to set the hearing threshold for the guard
    public float viewDis; //float variable to set the view distance for the guard
    public bool isPatrol; //bool variable to tell if the guard is patrolling or not
    public bool isChase; //bool variable to tell if the guard is chasing the player or not
    public bool inSight; //bool variable to tell if the player is in the guard's line of sight or not
    public float giveUpChaseDis; //float variable that tells the guard when the player gets far away enough that the guard should give up the chase
    public Image healthBar; //reference to the health bar Image on the guards canvas UI
    public int health; //int variable to hold the health of the guard 

    
    [SerializeField]
    public Transform pfFOV; //Transform reference to the prefab that holds the FOV object

    public FieldOfView fov; //reference to the FieldOfView script

    //virtual method that can be overridden by the children of the Pawn class
    public virtual void Move()
    {
        
    }

    //virtual method that can be overridden by the children of the Pawn class
    public virtual void TakeDamage(int damage)
    {

    }

    //virtual method that can be overridden by the children of the Pawn class
    public virtual void Patrol()
    {

    }

    //virtual method that can be overridden by the children of the Pawn class
    public virtual void FindNextWaypoint()
    {

    }

    //virtual method that can be overridden by the children of the Pawn class
    public virtual void Chase()
    {

    }

    //virtual method that can be overridden by the children of the Pawn class
    public virtual void FSM()
    {

    }
}
