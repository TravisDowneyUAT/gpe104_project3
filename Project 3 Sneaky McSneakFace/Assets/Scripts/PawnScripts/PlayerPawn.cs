﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPawn : Pawn
{
    public float moveSpeed; //float to hold the speed at which the player will move
    public float moveVol; //float to hold the volume the player will make while moving
    [HideInInspector]
    public NoiseControl noise; //reference to the NoiseControl script on the player prefab
   
    private void Start()
    {
        //setting the tf variable to the Transform of the player prefab
        tf = gameObject.GetComponent<Transform>();
        //setting the noise variable to the NoiseControl of the player prefab
        noise = gameObject.GetComponent<NoiseControl>();
    }

    public override void Move()
    {
        //setting floats for the Input of the player on the Horizontal and Vertical axes multiplied by the moveSpeed variable
        float translationY = Input.GetAxis("Horizontal") * moveSpeed;
        float translationX = Input.GetAxis("Vertical") * moveSpeed;

        //moving the player prefab by the player input on the X and Y axes
        tf.Translate(translationX, 0, 0);
        tf.Translate(0, translationY, 0);

        //saying that if the player has a noise component set the volume variable of the noise component
        //to whichever is higher, either the moveVol variable in the this script or the volume variable of the noise component
        if(noise != null)
        {
            noise.volume = Mathf.Max(noise.volume, moveVol);
        }
    }
}
