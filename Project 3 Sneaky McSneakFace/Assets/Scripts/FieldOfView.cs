﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    [SerializeField]
    private LayerMask layerMask; //setting a private layer mask variable

    private Mesh mesh; //setting a private mesh variable
    private Vector3 origin; //setting a private Vector3 variable
    private float startingAngle; //setting a private float variable to hold the starting angle
    private float fov; //setting a private float variable to hold the FOV

    // Start is called before the first frame update
    void Start()
    {
        mesh = new Mesh(); //set the mesh variable equal to a new Mesh component
        fov = 50f; //set the FOV variable to 50
        GetComponent<MeshFilter>().mesh = mesh; //set the mesh on the MeshFilter component of the game object to the newly created mesh
        origin = Vector3.zero; //set the origin vector3 variable to Vector.zero (which is equal to saying the origin is equal to origin)

    }

    // Update is called once per frame
    void Update()
    {
        int rayCount = 40; //declare a new int to hold the amount of rays fired to make the view cone
        float angle = startingAngle; //declare a new angle variable to hold the starting angle
        float angleInc = fov / rayCount; //increase the angle based on the FOV variable divided by the rayCount variable
        float viewDis = 5f; //declare a new view distance variable and set it equal to 5

        /*Create 3 new arrays,
         one Vector3,
         one Vector2,
         and one int.
         The Vector3 array will hold the vertices and will have a length of the rayCount variable plus 2.
         The Vector2 array will hold the uv and have a length equal the length of the vertices array.
         The int aray will hold the triangles and have a length equal to the rayCount variable multiplied by 3*/
        Vector3[] vertices = new Vector3[rayCount + 2];
        Vector2[] uv = new Vector2[vertices.Length];
        int[] triangle = new int[rayCount * 3];

        vertices[0] = origin; //set the first object of the vertices array equal to the origin variable

        int vertexIndex = 1; //create a new int variable to hold the index of the vertices array
        int triangleIndex = 0; //create a new int variable to hold the index of the triangles array
        /*run a for loop that will cast rays to make the view cone based on what we set rayCount to earlier
         create a new Vector3 variable and set it equal to the origin variable 
         plus an angle converted to a vetor multiplied by the view distance
         add the vertex vector to the vertices array based on the vertexIndex variable*/
        for (int i = 0; i <= rayCount; i++)
        {
            Vector3 vertex;
            RaycastHit2D hit = Physics2D.Raycast(origin, GetVectorFromAngle(angle), viewDis, layerMask);
            if (hit.collider == null)
            {
                vertex = origin + GetVectorFromAngle(angle) * viewDis;
            }
            else
            {
                vertex = hit.point;
            }
            vertices[vertexIndex] = vertex;

            /*run an if statement that says if the i variable from the for loop is greater than 0
             make the first object in the triangles array equal to 0
             the second object in the triangles array equal to the vertexIndex minus 1
             and the third object in the triangles array equal to the vertexIndex
             finally increase the triangleIndex variable by 3*/
            if (i > 0)
            {
                triangle[triangleIndex + 0] = 0;
                triangle[triangleIndex + 1] = vertexIndex - 1;
                triangle[triangleIndex + 2] = vertexIndex;

                triangleIndex += 3;
            }

            //increase the vertex index by 1
            vertexIndex++;
            //subtract the angle increase from the starting angle
            angle -= angleInc;
        }

        mesh.vertices = vertices; //set the vertices of the mesh to the vertices in the vertices array
        mesh.uv = uv; //set the uv of the mesh to the uv in the uv array
        mesh.triangles = triangle; //set the triangles of the mesh to the triangles in the triangles array
    }

    /*Set the origin of the FOV mesh based on whatever the origin is on the game object we're attached the 
     FOV mesh to*/
    public void SetOrigin(Vector3 origin)
    {
        this.origin = origin;
    }

    /*set the aim direction based on angle from a Vector3 minus the FOV divided by 2*/
    public void SetAimDirection(Vector3 aimDir)
    {
        startingAngle = GetAngleFromVectorFloat(aimDir) - fov / 2f;
    }

    public static Vector3 GetVectorFromAngle(float angle)
    {
        /*make a new float to hold the angle in radians and then convert the angle to radians by multiplying it
         by pi divided by 180
         then return the new vector3 based on the cosine of the radian angle and the the sine of the radian angle*/
        float angleRad = angle * (Mathf.PI / 180f);
        return new Vector3(Mathf.Cos(angleRad), Mathf.Sin(angleRad));
    }

    public static float GetAngleFromVectorFloat(Vector3 dir)
    {
        /*set the vector3 parameter to the normalized version of the parameter
         then create a new float to hold the vector3 after it has been converted to degrees
         if the new degrees is less than 0 then increase it by 360
         and finally return the float variable*/
        dir = dir.normalized;
        float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        if (n < 0)
        {
            n += 360;
        }
        return n;
    }
}
