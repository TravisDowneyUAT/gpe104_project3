﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DocumentPickup : MonoBehaviour
{
    public GameManager manager; //reference to the game manager

    private void OnTriggerEnter2D(Collider2D collision)
    {
        /*if the player collides with the collider on the document then deactivate the game object
         and run the DecreaseDocs method from the game manager*/
        if (collision.gameObject.tag.Equals("Player"))
        {
            gameObject.SetActive(false);
            manager.DecreaseDocs();
        }
    }
}
