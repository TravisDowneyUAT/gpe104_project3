﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseControl : MonoBehaviour
{
    public float volume;
    public float decayPerFrame = 0.016f;

    // Update is called once per frame
    void Update()
    {
        if (volume > 0)
        {
            volume -= decayPerFrame;
        }
    }
}
