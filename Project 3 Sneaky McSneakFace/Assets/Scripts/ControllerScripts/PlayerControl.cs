﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : Controller
{
    [HideInInspector]
    public Pawn pawn; //reference to a pawn object

    // Start is called before the first frame update
    void Start()
    {
        //setting the reference to the pawn object to get the PlayerPawn script information from the player prefab
        pawn = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerPawn>();
    }

    // Update is called once per frame
    void Update()
    {
        //calling the Move method in the player pawn to get player input
        pawn.Move();   
    }
}
