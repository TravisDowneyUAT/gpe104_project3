﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AIControl : Controller
{
    [HideInInspector]
    public Pawn pawn; //reference to the chosen pawn

    // Start is called before the first frame update
    void Start()
    {
        //setting the referene to get the AIPawn component of the guard prefab
        pawn = GameObject.FindGameObjectWithTag("Guard").GetComponent<AIPawn>();
        
    }

    // Update is called once per frame
    void Update()
    {
        //running the FSM method from the AIPawn script
        pawn.FSM();
        //setting the origin of the fov script to the position of the guard prefab
        pawn.fov.SetOrigin(pawn.tf.position);
        //setting the aim direction of the fov script to the up position of the guard prefab
        pawn.fov.SetAimDirection(pawn.tf.up);
        //load the game over scene if the inSight bool in the AIPawn script is set to true
        if(pawn.inSight == true)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
}
